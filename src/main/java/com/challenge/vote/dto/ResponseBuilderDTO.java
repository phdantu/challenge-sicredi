package com.challenge.vote.dto;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class ResponseBuilderDTO {

    public ResponseEntity<ApiResponseDTO> buildResponse(
            int httpStatusCode, String tipo, String titulo, Object itens, BotaoDTO botaoDTOOk, BotaoDTO botaoDTOCancelar) {
        return new ApiResponseDTO.ApiResponseBuilder <> (httpStatusCode)
                .withTipo(tipo)
                .withTitullo(titulo)
                .withItens(Collections.singletonList(itens))
                .withBotaoOk(botaoDTOOk)
                .withBotaoCancelar(botaoDTOCancelar)
                .build();
    }

    public ResponseEntity<ApiResponseDTO> buildResponse(
            int httpStatusCode, String tipo, String titulo, Object itens) {
        return new ApiResponseDTO.ApiResponseBuilder <> (httpStatusCode)
                .withTipo(tipo)
                .withTitullo(titulo)
                .withItens(Collections.singletonList(itens))
                .build();
    }

    public ResponseEntity<ApiResponseDTO> buildResponse(
            int httpStatusCode, Object itens) {
        return new ApiResponseDTO.ApiResponseBuilder <> (httpStatusCode).withItens(Collections.singletonList(itens)).build();
    }
}
