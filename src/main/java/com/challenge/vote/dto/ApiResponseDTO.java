package com.challenge.vote.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import java.util.List;

@JsonPropertyOrder({ "tipo",  "titulo", "itens", "botaoOk", "botaoCancelar"})
@Getter
@Setter
public class ApiResponseDTO<T> {

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private final HttpHeaders httpHeaders;
    private final int httpStatusCode;
    private final String tipo;
    private final String titulo;
    private final List<T> itens;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("botaoOk")
    private final BotaoDTO botaoDTOOk;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("botaoCancelar")
    private final BotaoDTO botaoDTOCancelar;

    private ApiResponseDTO(ApiResponseBuilder builder){
        this.httpHeaders = builder.httpHeaders;
        this.httpStatusCode = builder.httpStatusCode;
        this.tipo = builder.tipo;
        this.titulo = builder.titulo;
        this.itens = (List<T>) builder.itens;
        this.botaoDTOOk = builder.botaoDTOOk;
        this.botaoDTOCancelar = builder.botaoDTOCancelar;
    }

    public static class ApiResponseBuilder<T> {

        private HttpHeaders httpHeaders = new HttpHeaders();
        private int httpStatusCode;
        private String tipo;
        private String titulo;
        private List<T> itens;
        private BotaoDTO botaoDTOOk;
        private BotaoDTO botaoDTOCancelar;

        public ApiResponseBuilder<T> withHttpHeader(HttpHeaders httpHeader) {
            this.httpHeaders = httpHeader;
            return this;
        }

        public ApiResponseBuilder(int httpStatusCode) {
            this.httpStatusCode = httpStatusCode;
        }

        public ApiResponseBuilder<T> withTipo(String tipo) {
            this.tipo = tipo;
            return this;
        }

        public ApiResponseBuilder<T> withTitullo(String titulo) {
            this.titulo = titulo;
            return this;
        }

        public ApiResponseBuilder<T> withItens(List<T> itens) {
            this.itens = itens;
            return this;
        }

        public ApiResponseBuilder<T> withBotaoOk(BotaoDTO botaoDTOOk) {
            this.botaoDTOOk = botaoDTOOk;
            return this;
        }

        public ApiResponseBuilder<T> withBotaoCancelar(BotaoDTO botaoDTOCancelar) {
            this.botaoDTOCancelar = botaoDTOCancelar;
            return this;
        }

        public ResponseEntity<ApiResponseDTO> build() {
            ApiResponseDTO<T> apiResponseDTO = new ApiResponseDTO<>(this);
            return ResponseEntity.status(apiResponseDTO.getHttpStatusCode()).headers(apiResponseDTO.getHttpHeaders())
                    .body(apiResponseDTO);

        }
    }
}
