package com.challenge.vote.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BotaoDTO {

    private String texto;
    private String url;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonProperty("body")
    private BodyDTO bodyDTO;
}
