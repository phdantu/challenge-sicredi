package com.challenge.vote.dto;

import com.challenge.vote.entity.PautaEntity;
import com.challenge.vote.entity.enumerator.StatusEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PautaModelDTO {

    private String texto;
    private String url;

    public PautaModelDTO(PautaEntity entity, String url){
        this.texto = entity.getId().toString();
        this.url = url;
    }
}
