package com.challenge.vote.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BodyDTO {

    private String campo1;
    private Long campo2;
    private String idCampoTexto;
    private Long idCampoNumerico;
    private String idCampoData;

}
