package com.challenge.vote.controller;

import com.challenge.vote.dto.ApiResponseDTO;
import com.challenge.vote.dto.BodyDTO;
import com.challenge.vote.service.PautaService;
import com.challenge.vote.service.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(produces = "application/json")
public class PautaController {

    @Autowired
    private PautaService pautaService;

    @Autowired
    private VoteService voteService;

    @GetMapping(value = "/pautas")
    public ResponseEntity<ApiResponseDTO> getPautas(HttpServletRequest request){
        return pautaService.getPautas(request);
    }

    @GetMapping(value = "/pauta/{id}")
    public ResponseEntity<ApiResponseDTO> getPauta(HttpServletRequest request, @PathVariable("id") Long id) throws Exception {
        return pautaService.getPauta(request, id);
    }


    @PostMapping(value = "/newPauta")
    public ResponseEntity<ApiResponseDTO> saveNewPauta(HttpServletRequest request, @Valid @RequestBody BodyDTO data){
        return pautaService.createNewPauta(request, data);
    }

    //Temporariamente neste Controller por ser unico metodo
    @PostMapping(value = "/vote", consumes = "application/json")
    public ResponseEntity<ApiResponseDTO> saveNewVote(@Valid @RequestBody BodyDTO data) throws Exception {
        return voteService.registerNewVote(data);
    }

}
