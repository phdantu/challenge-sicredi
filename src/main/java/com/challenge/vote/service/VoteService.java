package com.challenge.vote.service;

import com.challenge.vote.dto.ApiResponseDTO;
import com.challenge.vote.dto.ResponseBuilderDTO;
import com.challenge.vote.dto.BodyDTO;
import com.challenge.vote.dto.BotaoDTO;
import com.challenge.vote.dto.VoteModelDTO;
import com.challenge.vote.entity.PautaEntity;
import com.challenge.vote.entity.VoteEntity;
import com.challenge.vote.entity.enumerator.VoteEnum;
import com.challenge.vote.external.api.ApiConsumer;
import com.challenge.vote.repository.VoteRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class VoteService {

    @Autowired
    private VoteRepository voteRepository;

    @Autowired
    private PautaService pautaService;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ResponseBuilderDTO responseBuilderDTO;

    @Autowired
    private ApiConsumer apiConsumer;

    public ResponseEntity<ApiResponseDTO> registerNewVote(BodyDTO data) throws Exception {
        var pauta = pautaService.checkIfSessionIsAvailable(data.getIdCampoNumerico());



        //Deveria chamar a API externa verifyCpf() porem o servico nao foi localizado
        //coloquei este metodo para simular uma validacao ficticia
        verifyIfHaventVotedYet(pauta, data.getIdCampoTexto());


        var entity = voteRepository.save(VoteEntity.builder()
                                    .vote(VoteEnum.valueOf(data.getCampo1()))
                                    .pauta(pauta)
                                    .userIdCpf(data.getIdCampoTexto())
                                    .build());

        return responseBuilderDTO.buildResponse(HttpStatus.OK.value(),
                "FORMULARIO", "TITULO TELA",
                toVoteModelDTO(entity) , new BotaoDTO("Ação 1", "URL", new BodyDTO()), new BotaoDTO("Cancelar", "URL", null));
    }

    private void verifyIfHaventVotedYet(PautaEntity pauta, String cpf) throws Exception {
        verifyCpf(cpf);
        var vote = voteRepository.findByPautaAndUserIdCpf(pauta, cpf);

        if(vote.isPresent()){
            throw new Exception("Este associado já votou nesta sessão!");
        }
    }

    public String verifyCpf(String cpf) throws Exception {
        //apiConsumer.verifyCpf(cpf);
        Long cpfNumber = Long.valueOf(cpf);
        return cpf;
    }

    private VoteModelDTO toVoteModelDTO(VoteEntity entity){
        return modelMapper.map(entity, VoteModelDTO.class);
    }

}
