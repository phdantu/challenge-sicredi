package com.challenge.vote.service;

import com.challenge.vote.dto.ApiResponseDTO;
import com.challenge.vote.dto.ResponseBuilderDTO;
import com.challenge.vote.dto.BodyDTO;
import com.challenge.vote.dto.BotaoDTO;
import com.challenge.vote.dto.PautaModelDTO;
import com.challenge.vote.entity.PautaEntity;
import com.challenge.vote.entity.enumerator.StatusEnum;
import com.challenge.vote.entity.enumerator.VoteEnum;
import com.challenge.vote.repository.PautaRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.NotFoundException;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class PautaService {
    
    @Autowired
    private PautaRepository pautaRepository;

    @Autowired
    private ResponseBuilderDTO responseBuilderDTO;

    public ResponseEntity<ApiResponseDTO> createNewPauta(HttpServletRequest request, BodyDTO data) {
        Long duration = (data.getCampo2() != null && data.getCampo2() >= 1L) ? data.getCampo2() : 1L;
        var pauta = pautaRepository.save(PautaEntity.builder()
                .status(StatusEnum.ABERTA)
                .finishesAt(LocalDateTime.now().plusMinutes(duration))
                .build());
        return responseBuilderDTO.buildResponse(HttpStatus.CREATED.value(),
                new PautaModelDTO(pauta, request.getHeader(HttpHeaders.HOST) + "/pauta/" + pauta.getId()));
    }

    public ResponseEntity<ApiResponseDTO> getPautas(HttpServletRequest request){
        List<PautaModelDTO> pautas = toPautaModelDTO(pautaRepository.findAll(), request.getHeader(HttpHeaders.HOST) + "/pauta/");

        return responseBuilderDTO.buildResponse(HttpStatus.OK.value(),
                "FORMULARIO", "TITULO TELA", pautas);
    }

    public ResponseEntity<ApiResponseDTO> getPauta(HttpServletRequest request, Long id) throws Exception {
        if(id == null){
            throw new Exception("ID não informado");
        }
        var pauta = this.findPauta(id);

        return responseBuilderDTO.buildResponse(HttpStatus.OK.value(),
                "FORMULARIO", "TITULO TELA", pauta,
                new BotaoDTO("Ação 1", request.getHeader(HttpHeaders.HOST) + "/pauta/" + pauta.getId(), new BodyDTO()),
                new BotaoDTO("Cancelar", "URL", null));
    }

    public PautaEntity findPauta(Long pautaId) {
        return pautaRepository.findById(pautaId).orElseThrow(() -> new NotFoundException("Pauta não encontrada."));
    }

    public PautaEntity checkIfSessionIsAvailable(Long pautaId) throws Exception {
        var pauta = findPauta(pautaId);

        if(pauta.getStatus().equals(StatusEnum.ENCERRADA)){
            throw new Exception("Sessão já encerrada!");
        }

        return pauta;
    }

    @Scheduled(fixedDelay = 10000)
    public void verifyOpenSession() {
        log.info("Verify for open Pautas");
        List<PautaEntity> pautas = pautaRepository.findByStatusAndFinishesAtLessThan(StatusEnum.ABERTA, LocalDateTime.now());
        if(!pautas.isEmpty()){
            pautas.forEach( pautaEntity -> {
                        if(pautaEntity.getVotes() != null){
                            try {
                                log.info("Closing Pautas and saving the result");
                                pautaEntity.setStatus(StatusEnum.ENCERRADA);
                                pautaEntity.setResult(defineVotingResult(pautaEntity));
                            } catch (Exception e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }
            );
            pautaRepository.saveAll(pautas);
        }
        log.info("End of Verifications.");
    }

    /**
     * Metodo para definir o resultado da votacao e devolver uma string de acordo
     *
     * @param pautaEntity
     * @return
     */
    private String defineVotingResult(PautaEntity pautaEntity) {
        Integer totalVotes = pautaEntity.getVotes().size();
        if(totalVotes > 0){
            Long totalSimVotes = pautaEntity.getVotes().stream().filter(voteEntity -> voteEntity.getVote().equals(VoteEnum.SIM)).count();
            Long totalNaoVotes = totalVotes - totalSimVotes;
            DecimalFormat numberFormat = new DecimalFormat("#.##");
            String percentageOfSim = numberFormat.format((double) ((totalSimVotes*100)/totalVotes));
            String percentageOfNao = numberFormat.format((double) ((totalNaoVotes*100)/totalVotes));
            return "SIM: " + percentageOfSim + "% - NAO: " + percentageOfNao + "%";
        }
        return "SIM: 0% - NAO: 0%";
    }

    /**
     * Mapear uma lista de entidades para uma lista de DTOs
     *
     * @param entities
     * @param url
     * @return
     */
    private List<PautaModelDTO> toPautaModelDTO(List<PautaEntity> entities, String url){
        List<PautaModelDTO> dtos = new ArrayList<>();
        entities.forEach(entity -> {
                dtos.add(new PautaModelDTO(entity, url + entity.getId()));
            }
        );
        return dtos;
    }

}
