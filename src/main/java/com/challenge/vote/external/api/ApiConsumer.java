package com.challenge.vote.external.api;

import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class ApiConsumer {

    public String verifyCpf(String cpf){
        String uri = "https://user-info.herokuapp.com/users/" + cpf;
        RestTemplate restTemplate = new RestTemplate();
        try{
            return restTemplate.getForObject(uri, String.class);
        }catch (HttpClientErrorException e){
            e.printStackTrace();
            return "CPF INVALIDO";
        }
    }

}