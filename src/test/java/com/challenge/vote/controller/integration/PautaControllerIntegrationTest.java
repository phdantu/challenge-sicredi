package com.challenge.vote.controller.integration;

import com.challenge.vote.dto.BodyDTO;
import com.challenge.vote.dto.VoteDTO;
import com.challenge.vote.entity.PautaEntity;
import com.challenge.vote.entity.enumerator.StatusEnum;
import com.challenge.vote.entity.enumerator.VoteEnum;
import com.challenge.vote.repository.PautaRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import javax.ws.rs.NotFoundException;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@AutoConfigureMockMvc
public class PautaControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PautaRepository pautaRepository;

    @Test
    void whenTryFindPautaWithNullPaudaId() throws Exception {

        assertThrows(IllegalArgumentException.class, () ->{
            try {
                mockMvc.perform(put("/pauta/{id}", null)
                        .contentType("application/json")
                        .accept(MediaType.ALL)).andDo(print()).andReturn();
            }catch (NestedServletException e) {
                throw e.getCause();}
        });
    }


    @Test
    void whenTryToVoteWithInvalidPautaId() throws Exception {
        var json = BodyDTO.builder().campo1(VoteEnum.SIM.name()).idCampoNumerico(93670170065L).build();

        assertThrows(NotFoundException.class, () ->{
            try {
                mockMvc.perform(post("/vote")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(json))
                        .accept(MediaType.ALL)).andDo(print()).andReturn();
            }catch (NestedServletException e) {
                throw e.getCause();}
        });
    }

    @Test
    void whenTryToVoteWithInvalidCpfAssertErrorMessage() throws Exception {
        var entity = pautaRepository.save(PautaEntity.builder()
                .finishesAt(LocalDateTime.now().plusMinutes(10L))
                .status(StatusEnum.ABERTA).build());

        var json = BodyDTO.builder().campo1(VoteEnum.SIM.toString())
                .idCampoTexto("s").idCampoNumerico(entity.getId()).build();

        assertThrows(NumberFormatException.class, () ->{
            try {
                mockMvc.perform(post("/vote")
                        .contentType("application/json")
                        .content(objectMapper.writeValueAsString(json))
                        .accept(MediaType.ALL)).andDo(print()).andReturn();
            }catch (NestedServletException e) {
                throw e.getCause();
            }
        });

        pautaRepository.delete(entity);
    }


    @Test
    void whenTryToVoteWhenSessionIsClosed() throws Exception {
        var entity = pautaRepository.save(PautaEntity.builder()
                .status(StatusEnum.ENCERRADA).build());

        var json = BodyDTO.builder().campo1(VoteEnum.SIM.name()).idCampoNumerico(entity.getId()).build();

        String message = null;

        try {
            mockMvc.perform(post("/vote")
                    .contentType("application/json")
                    .content(objectMapper.writeValueAsString(json))
                    .accept(MediaType.ALL)).andDo(print()).andReturn();
        }catch (NestedServletException e) {
            message = (String) e.getMessage();
        }

        assertTrue(message.contains("Sessão já encerrada!"));

        pautaRepository.delete(entity);
    }

}
